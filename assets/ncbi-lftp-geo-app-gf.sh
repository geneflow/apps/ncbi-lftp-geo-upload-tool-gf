#!/bin/bash

# LFTP app to upload geo data folder to NCBI wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## MODIFY >>> *****************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --local_dir => Directory where read files are located"
    echo "  --ftp_site => NCBI ftp link"
    echo "  --user => User name for ftp submission"
    echo "  --password => Password for ftp submission"
    echo "  --remote => directory for submission"
    echo "  --output_dir => Name of remote directory to upload files"
    echo "  --exec_method => Execution method (environment, auto)"
    echo "  --help => Display this help message"
}
## ***************************************************************** <<< MODIFY

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd"
    ERROR_CODE=$?
    if [ ${ERROR_CODE} -ne 0 ]; then
        echo "Error when executing command '${cmd}'"
        exit ${ERROR_CODE}
    fi
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## MODIFY >>> *****************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,local_dir:,ftp_site:,user:,password:,remote:,output_dir:,
## ***************************************************************** <<< MODIFY

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## MODIFY >>> *****************************************************************
## Set any defaults for command line options
EXEC_METHOD="auto"
## ***************************************************************** <<< MODIFY

## MODIFY >>> *****************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --local_dir)
            if [ -z "${local_dir}" ]; then
                LOCAL_DIR=$2
            else
                LOCAL_DIR=${local_dir}
            fi
            shift 2
            ;;
        --ftp_site)
            if [ -z "${ftp_site}" ]; then
                FTP_SITE=$2
            else
                FTP_SITE=${ftp_site}
            fi
            shift 2
            ;;
        --user)
            if [ -z "${user}" ]; then
                USER=$2
            else
                USER=${user}
            fi
            shift 2
            ;;
        --password)
            if [ -z "${password}" ]; then
                PASSWORD=$2
            else
                PASSWORD=${password}
            fi
            shift 2
            ;;
        --remote)
            if [ -z "${remote}" ]; then
                REMOTE=$2
            else
                REMOTE=${remote}
            fi
            shift 2
            ;;
        --output_dir)
            if [ -z "${output_dir}" ]; then
                OUTPUT_DIR=$2
            else
                OUTPUT_DIR=${output_dir}
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD=${exec_method}
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ***************************************************************** <<< MODIFY

## MODIFY >>> *****************************************************************
## Log any variables passed as inputs
echo "Local_dir: ${LOCAL_DIR}"
echo "Ftp_site: ${FTP_SITE}"
echo "User: ${USER}"
echo "Password: ${PASSWORD}"
echo "Remote: ${REMOTE}"
echo "Output_dir: ${OUTPUT_DIR}"
echo "Execution Method: ${EXEC_METHOD}"
## ***************************************************************** <<< MODIFY



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# LOCAL_DIR input

if [ -z "${LOCAL_DIR}" ]; then
    echo "Directory where read files are located required"
    echo
    usage
    exit 1
fi
# make sure LOCAL_DIR is staged
count=0
while [ ! -d "${LOCAL_DIR}" ]
do
    echo "${LOCAL_DIR} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${LOCAL_DIR}" ]; then
    echo "Directory where read files are located not found: ${LOCAL_DIR}"
    exit 1
fi
LOCAL_DIR_FULL=$(readlink -f "${LOCAL_DIR}")
LOCAL_DIR_DIR=$(dirname "${LOCAL_DIR_FULL}")
LOCAL_DIR_BASE=$(basename "${LOCAL_DIR_FULL}")



# FTP_SITE parameter
if [ -n "${FTP_SITE}" ]; then
    :
else
    :
    echo "NCBI ftp link required"
    echo
    usage
    exit 1
fi


# USER parameter
if [ -n "${USER}" ]; then
    :
else
    :
    echo "User name for ftp submission required"
    echo
    usage
    exit 1
fi


# PASSWORD parameter
if [ -n "${PASSWORD}" ]; then
    :
else
    :
    echo "Password for ftp submission required"
    echo
    usage
    exit 1
fi


# REMOTE parameter
if [ -n "${REMOTE}" ]; then
    :
else
    :
    echo "directory for submission required"
    echo
    usage
    exit 1
fi


# OUTPUT_DIR parameter
if [ -n "${OUTPUT_DIR}" ]; then
    :
else
    :
fi


## ***************************************************************** <<< MODIFY

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   package: binaries packaged with the app
##   cdc-shared-package: binaries centrally located at the CDC
##   singularity: singularity image packaged with the app
##   cdc-shared-singularity: singularity image centrally located at the CDC
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path
##   module: environment modules

## MODIFY >>> *****************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="environment auto"
## ***************************************************************** <<< MODIFY

# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## MODIFY >>> *****************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect if singularity available
    if command -v singularity >/dev/null 2>&1; then
        SINGULARITY=yes
    else
        SINGULARITY=no
    fi

    # detect if docker available
    if command -v docker >/dev/null 2>&1; then
        DOCKER=yes
    else
        DOCKER=no
    fi

    # detect execution method
    if command -v lftp >/dev/null 2>&1; then
        AUTO_EXEC=environment
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add logic to prepare environment for execution
## ***************************************************************** <<< MODIFY



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    environment)
        MNT=""; ARG=""; CMD0="cd ${READS} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        MNT=""; ARG=""; CMD0="lftp -c ' open -u ${USER},${PASSWORD} ${FTP_SITE}; cd uploads/${REMOTE}; mirror --reverse -L -v ${LOCAL_DIR} ${REMOTE_DIR};close;bye' ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ***************************************************************** <<< MODIFY



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ***************************************************************** <<< MODIFY

